# 美文阅读RSS转LaTeX脚本
# 2024.4.1 by Sion

# 项目流程
# 1. 数据清洗：将RSS输入源按照日期分类

import feedparser
import html2text
import os
import requests
from urllib.parse import urljoin
from os import makedirs
from os.path import exists, join
from time import strftime

more2read_rss_url = 'https://rsshub.rssforever.com/cgtn/podcast/ezfm/4'  # 此RSS源可以显示二十条信息
more2read_raw = feedparser.parse(more2read_rss_url)

print("RSS源版本：", more2read_raw.version)
print("抓取到的RSS数量：", len(more2read_raw.entries))

for i, entry in enumerate(more2read_raw.entries):
    print("开始输出第", i + 1, "个条目的信息")
    print('标题：', entry.title)
    print('发布日期：', entry.published)
    print('格式化的发布日期：', entry.published_parsed)
    print('封面图', entry.image)
    print('音频时长：', entry.itunes_duration)
    print("\n")

    # 对日期进行格式化
    def struct_time_to_formatted_date(time_struct):
        # 定义日期格式
        date_format = "%Y_%m_%d"
        # 使用strftime方法格式化时间
        return strftime(date_format, time_struct)
    def tex_struct_time_to_formatted_date(time_struct):
        # 定义日期格式
        date_format = "%m月%d日"
        # 使用strftime方法格式化时间
        return strftime(date_format, time_struct)
    # 将time.struct_time对象转换为格式化的日期字符串
    date = struct_time_to_formatted_date(entry.published_parsed)
    date_tex = tex_struct_time_to_formatted_date(entry.published_parsed)

    #下载封面
    cover_url = entry.image.href
    cover_filename = f"{date}.jpg"
    cover_directory = join('source', 'cover')
    cover_path = join(cover_directory, cover_filename)
    if not exists(cover_directory):
        makedirs(cover_directory)
    with requests.get(cover_url, stream=True) as r:
        if r.status_code == 200:
            with open(cover_path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
        else:
            print(f"Failed to download cover image: {cover_url}")


    # 使用html2text对html文本进行markdown化
    main_text_html = entry.summary
    main_text = html2text.html2text(main_text_html)

    # 按照"Daily Quote", "Poem of the Day", 和 "Beauty of Words"进行分割
    main_sort = {
        "Daily Quote": "",
        "Poem of the Day": "",
        "Beauty of Words": ""
    }

    start_positions = {
        "Daily Quote": main_text.find("Daily Quote"),
        "Poem of the Day": main_text.find("Poem of the Day"),
        "Beauty of Words": main_text.find("Beauty of Words")
    }

    # 处理每个章节的内容
    for section, start_pos in start_positions.items():
        # 如果找到了章节标题
        if start_pos != -1:
            # 跳过标题本身，找到正文的起始位置
            start_pos += len(section) + 1  # 加上标题长度和换行符
            # 找到下一个章节的起始位置，或者假设为文本的末尾
            next_section = min([pos for pos in start_positions.values() if pos > start_pos], default=len(main_text))
            # 截取当前章节的内容
            main_sort[section] = main_text[start_pos:next_section].strip()

    # 创建TeX文档
    relative_cover_path = join('cover', cover_filename)
    tex_content = ""
    tex_content += "\\section{" + date_tex + "}\n\n"
    tex_content += "\\dailyquote{" + main_sort["Daily Quote"] + "}{作者}{" + relative_cover_path + "}\n\n"
    tex_content += "\\poems{标题}{作者}{84mm} {" + main_sort["Poem of the Day"] + "}{82mm}{B内容}\n\n"
    tex_content += "% \\words{标题}{作者}{" + main_sort["Beauty of Words"] + "}\n"

    # 假设的TeX文件名
    tex_filename = f"{date}.tex"
    file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'source', tex_filename)

    # 将TeX内容写回到TeX文件中
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write(tex_content)

    print(f"TeX文档已生成：{tex_filename}")
